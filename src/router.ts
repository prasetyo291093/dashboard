import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

import Test from "./views/test.vue";

const routes: RouteRecordRaw[] = [
  {
    path: "/",
    name: "test",
    component: Test,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

export default router;
